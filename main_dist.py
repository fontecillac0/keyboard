#! /usr/bin/env python
import serial
import rospy
from std_msgs.msg import String
import RPi.GPIO as GPIO
import time

lec_encoders=[0.0,0.0,0.0,0.0]
 
#funcion que mete en un arreglo los 
#valores de los encoders
def solicitud_enc():
	#el arduino maestro cuando
	#lee una "a" solicita a sus
	#esclavos las vueltas
	dato.write("a")
	for r in range(4):
		lec_encoders[r]=dato.readline()
		lec_encoders[r]=lec_encoders[r].replace("\n","").replace("\r","")
		lec_encoders[r]=float(lec_encoders[r])	
	return lec_encoders

#salidas que controlan a los motores
def gpio_setup():
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(17, GPIO.OUT)
	GPIO.setup(27, GPIO.OUT)
	GPIO.setup(23, GPIO.OUT)
	GPIO.setup(24, GPIO.OUT)
	GPIO.setup(25, GPIO.OUT)
	GPIO.setup(22, GPIO.OUT)
	GPIO.setup(26, GPIO.OUT)
	GPIO.setup(16, GPIO.OUT)

#funciones que controlan cada motor
def mot1(dir):
	if dir == "forward":
		GPIO.output(17,1)
		GPIO.output(27,0)
	elif dir == "back":
		GPIO.output(17, 0)
		GPIO.output(27, 1)
	elif dir == "stop":
		GPIO.output(17, 0)
		GPIO.output(27, 0)
def mot2(dir):
	if dir == "forward":
		GPIO.output(23,1)
		GPIO.output(24,0)
	elif dir == "back":
		GPIO.output(23,0)
		GPIO.output(24,1)
	elif dir == "stop":
		GPIO.output(23,0)
		GPIO.output(24,0)
def mot3(dir):
	if dir == "forward":
		GPIO.output(25,1)
		GPIO.output(22,0)
	elif dir == "back":
		GPIO.output(25,0)
		GPIO.output(22,1)
	elif dir == "stop":
		GPIO.output(25,0)
		GPIO.output(22,0)
def mot4(dir):
	if dir == "forward":
		GPIO.output(26,1)
		GPIO.output(16,0)
	elif dir == "back":
		GPIO.output(26,0)
		GPIO.output(16,1)
	elif dir == "stop":
		GPIO.output(26,0)
		GPIO.output(16,0)

#funciones para direccionar al robot
def left():
	mot1("back")
	mot2("forward")
	mot3("forward")
	mot4("back")

def right():
	mot1("forward")
	mot2("back")
	mot3("back")
	mot4("forward")

def stop():
	mot1("stop")
	mot2("stop")
	mot3("stop")
	mot4("stop")

def forward():
	mot1("forward")
	mot2("forward")
	mot3("forward")
	mot4("forward")

def backward():
	mot1("back")
	mot2("back")
	mot3("back")
	mot4("back")

obstaculo = 0
#interrupcion si se publica un dato en /oscar_topic
def callback(msg):
	global obstaculo
	obstaculo = msg

#publica una letra en el canal /orientacion
def publicar_direccion(dir):
	direccion.data = dir
	pub.publish(direccion)

rospy.init_node("teleop_node")
#en este canal se publica si hay obstaculo o no
sub = rospy.Subscriber("/oscar_topic", oscar msg, callback)
#en este canal se publica la direccion del robot
pub = rospy.Publisher("/orientation",String,queue_size=1)
direccion = String()
gpio_setup()
#delay necesario para que funcione 
#uart correctamente (no se xq)
time.sleep(2)
while not rospy.is_shutdown():
	while obstaculo ==:
	#el arduino maestro cuando recibe una
	#"b" le dice a los esclavos que 
	#empiecen a contar de 0
		dato.write("b")
		time.sleep(.5)
		print("init: "+str(solicitud_enc()))
		right()
		publicar_direccion("r")
		time.sleep(7)
		stop()
		print("right: "+str(solicitud_enc()))
		forward()
		publicar_direccion("f")
		time.sleep(7)
		stop()
		print("forward: "+str(solicitud_enc()))
		backward()
		publicar_direccion("b")
		time.sleep(7)
		stop()
		print("back: "+str(solicitud_enc()))
		left()
		publicar_direccion("l")
		time.sleep(7)
		stop()
		print("left: "+str(solicitud_enc()))
		time.sleep(1)
	stop()
