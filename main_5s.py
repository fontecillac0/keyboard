#! /usr/bin/env python
import serial
import rospy
from geometry_msgs.msg import Twist
import RPi.GPIO as GPIO
import time

x = 0
y = 0
lec_encoders=[0.0,0.0,0.0,0.0] 
def solicitud_enc():
	dato.write("a")
	for r in range(4):
		lec_encoders[r]=dato.readline()
		lec_encoders[r]=lec_encoders[r].replace("\n","").replace("\r","")
		lec_encoders[r]=float(lec_encoders[r])	
	return lec_encoders
def gpio_setup():
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(12,GPIO.OUT)
	global motor
	motor = GPIO.PWM(12,100)
	motor.start(0)
	GPIO.setup(17, GPIO.OUT)
	GPIO.setup(27, GPIO.OUT)
	GPIO.setup(23, GPIO.OUT)
	GPIO.setup(24, GPIO.OUT)
	GPIO.setup(25, GPIO.OUT)
	GPIO.setup(22, GPIO.OUT)
	GPIO.setup(26, GPIO.OUT)
	GPIO.setup(16, GPIO.OUT)
def mot1(dir):
	if dir == "forward":
		GPIO.output(17,1)
		GPIO.output(27,0)
	elif dir == "back":
		GPIO.output(17, 0)
		GPIO.output(27, 1)
	elif dir == "stop":
		GPIO.output(17, 0)
		GPIO.output(27, 0)
def mot2(dir):
	if dir == "forward":
		GPIO.output(23,1)
		GPIO.output(24,0)
	elif dir == "back":
		GPIO.output(23,0)
		GPIO.output(24,1)
	elif dir == "stop":
		GPIO.output(23,0)
		GPIO.output(24,0)
def mot3(dir):
	if dir == "forward":
		GPIO.output(25,1)
		GPIO.output(22,0)
	elif dir == "back":
		GPIO.output(25,0)
		GPIO.output(22,1)
	elif dir == "stop":
		GPIO.output(25,0)
		GPIO.output(22,0)
def mot4(dir):
	if dir == "forward":
		GPIO.output(26,1)
		GPIO.output(16,0)
	elif dir == "back":
		GPIO.output(26,0)
		GPIO.output(16,1)
	elif dir == "stop":
		GPIO.output(26,0)
		GPIO.output(16,0)
def left(dc):
	mot1("back")
	mot2("forward")
	mot3("forward")
	mot4("back")
	motor.ChangeDutyCycle(dc)
def right(dc):
	mot1("forward")
	mot2("back")
	mot3("back")
	mot4("forward")
	motor.ChangeDutyCycle(dc)
def stop():
	mot1("stop")
	mot2("stop")
	mot3("stop")
	mot4("stop")
def forward(dc):
	mot1("forward")
	mot2("forward")
	mot3("forward")
	mot4("forward")
	motor.ChangeDutyCycle(dc)

def backward(dc):
	mot1("back")
	mot2("back")
	mot3("back")
	mot4("back")
	motor.ChangeDutyCycle(dc)

def callback(msg):
	lineal = msg.linear
	global x 
	global y 
	x = lineal.x
	y = lineal.y 

rospy.init_node("teleop_node")
dato=serial.Serial('/dev/ttyUSB0', 9600)
sub = rospy.Subscriber("/cmd_vel", Twist, callback)
gpio_setup()
rate = rospy.Rate(4)
time.sleep(2)

dato.write("b")
time.sleep(.5)
print("init: "+str(solicitud_enc()))
right(0)
time.sleep(7)
stop()
print("right: "+str(solicitud_enc()))
forward(0)
time.sleep(7)
stop()
print("forward: "+str(solicitud_enc()))
backward(0)
time.sleep(7)
stop()
print("back: "+str(solicitud_enc()))
left(0)
time.sleep(7)
stop()
print("left: "+str(solicitud_enc()))
arr = solicitud_enc()
print(arr[0]+10)
